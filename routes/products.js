var express = require('express');
var router = express.Router();

const admin = require('firebase-admin');

var db = admin.firestore();

/* POST add product. */
router.post('/create', function (req, res, next) {

    const data = {
        'name': req.body.name,
        'manufactory': req.body.manufactory,
        'productId': req.body.productId,
        'msds': req.body.msds,
    };

    var createdProduct = db.collection('products').doc(data.productId).set(data);

    res.send(data);
});

/* GET products listing. */
router.get('/listing', function (req, res, next) {

    var productsRef = db.collection('products');

    var allCities = productsRef.get()
        .then(snapshot => {

            let data = [];

            snapshot.forEach(doc => {
                console.log(doc.id, '=>', doc.data());
                let temp = doc.data();

                data.push(temp);
            });

            res.send(data);
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });
});

/* POST update product. */
router.post('/update', function (req, res, next) {

    const data = {
        'name': req.body.name,
        'manufactory': req.body.manufactory,
        'productId': req.body.productId,
        'msds': req.body.msds,
    };

    var createdProduct = db.collection('products').doc(data.productId).set(data);

    res.send(data);
});

/* GET remove product. */
router.delete('/delete', function (req, res, next) {

    var createdProduct = db.collection('products').doc(req.body.productId).delete();

    res.send('succuss');
});


module.exports = router;
