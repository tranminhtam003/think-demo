var express = require('express');
var router = express.Router();

const admin = require('firebase-admin');

var serviceAccount = require("../think-e7103-firebase-adminsdk-nbs6z-9edf16214e.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

/* config something */
var db = admin.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
db.settings(settings);


/* GET users listing. */
router.get('/demo', function(req, res, next) {
  var aTuringRef = db.collection('users').doc('aturing');

  var setAlan = aTuringRef.set({
    'first': 'Alan',
    'middle': 'Mathison',
    'last': 'Turing',
    'born': 1912
  });
  
  res.send(setAlan);
});

/* GET users listing. */
router.get('/yyy', function(req, res, next) {
  res.send('My name Tim Rau');
});

module.exports = router;
